# pgsodium

This is a fork of [https://github.com/michelp/pgsodium](https://github.com/michelp/pgsodium).

This fork includes the functions `crypto_sign_detached` and `crypto_sign_verify_detached`, which were not included in the original `pgsodium` repo.
